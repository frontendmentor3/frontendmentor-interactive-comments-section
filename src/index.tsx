import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import {GlobalStyles} from './styles/GlobalStyles';
import {reducer, StateProvider} from './state';


ReactDOM.render(
    <StateProvider reducer={reducer}>
        <>
            <GlobalStyles/>
            <App/>
        </>

    </StateProvider>,
    document.getElementById('root')
);
