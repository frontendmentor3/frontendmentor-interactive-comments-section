import data from '../data/data.json'
import {CommentType, UserType} from '../types/MessageData';


export const getCurrentUser = (): UserType => {
    return data.currentUser
}

export const getComments = (): CommentType[] => {
    return data.comments
}
