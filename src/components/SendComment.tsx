import React, {useState} from 'react'
import {useStateValue} from '../state';
import {getAvatarFileName} from '../utils/getAvatarFilename';
import {addComment} from '../state';
import {EditCommentStyles} from '../styles/EditCommentStyles';
import styled from 'styled-components';

const SendCommentStyles = styled(EditCommentStyles)`
  width: 90%;
  max-width: 900px;
  margin: 10px 0 40px 0;
`

const SendComment = () => {
    const [{currentUser},] = useStateValue()
    const [comment, setComment] = useState('')
    const [, dispatch] = useStateValue()

    const handleSend = () => {
        const newComment = {
            id: Math.floor(Math.random() * 1000000),
            content: comment,
            createdAt: 'now',
            score: 0,
            user: currentUser!!,
            replies: []
        }
        dispatch(addComment(newComment))
        setComment('')
    }

    return (
        <SendCommentStyles>
            {currentUser ? (<>
                    <img className="avatar"
                         src={`/assets/images/avatars/${getAvatarFileName(currentUser.image.png)}`}
                         alt={`${currentUser.username}-avatar`}/>
                    <textarea
                        onChange={(e) => setComment(e.target.value)}
                        value={comment}
                        placeholder="Add a comment..."></textarea>
                    <div className="send-btn" onClick={handleSend}>SEND</div>
                </>)
                : "You're not logged in."}
        </SendCommentStyles>
    )

}

export default SendComment