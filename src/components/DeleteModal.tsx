import React from 'react'
import styled from 'styled-components';
import {deleteComment, useStateValue} from '../state';


type DeleteModalPropsType = {
    setShow: React.Dispatch<React.SetStateAction<boolean>>
    commentId: number
    level: number
}
const DeleteModalStyles = styled.div`
  line-height: 1.5rem;
  width: 330px;
  background-color: var(--white);
  padding: 20px 40px;
  border-radius: 10px;
  position: fixed;
  left: 50%;
  top: 50%;
  margin-left: -200px;
  z-index: 1;


  h1 {
    color: var(--secondary-dark-blue);
    font-size: 1.5rem;
    font-weight: bold;
  }

  .no-btn, .yes-btn {
    padding: 20px;
    text-transform: uppercase;
    font-size: 1.2rem;
    color: var(--white);
    border-radius: 10px;
    cursor: pointer;
  }

  .delete-msg {
    margin-bottom: 20px;
    font-size: 1.1rem;
    line-height: 2rem;
    color: var(--secondary-grayish-blue);
  }

  .btns {
    display: flex;
    justify-content: space-between;
  }

  .no-btn {
    background-color: var(--secondary-grayish-blue);
  }

  .yes-btn {
    background-color: var(--primary-soft-red);
  }
`

export const DeleteModal = ({setShow, commentId, level}: DeleteModalPropsType) => {
    const [, dispatch] = useStateValue()

    return (
        <DeleteModalStyles>
            <h1>Delete comment</h1>
            <div className="delete-msg">Are you sure you want to delete this comment? This will remove the comment and
                can't be undone.
            </div>
            <div className="btns">
                <div className="no-btn" onClick={() => setShow(false)}>no, cancel</div>
                <div className="yes-btn" onClick={() => {
                    dispatch(deleteComment(commentId, level))
                    setShow(false)
                }}>yes, delete
                </div>
            </div>
        </DeleteModalStyles>
    )

}