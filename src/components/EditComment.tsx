import React, {useState} from 'react'
import {EditCommentStyles} from '../styles/EditCommentStyles';
import styled from 'styled-components';
import {devices} from '../styles/Sizes';
import {editComment, useStateValue} from '../state';

type EditCommentPropsType = {
    id: number,
    content: string,
    replyingTo: string | undefined,
    commentLevel: number,
    stopEditing: () => void
}

const EditCommentContainerStyles = styled(EditCommentStyles)`
  grid-area: content;
  padding: 0px;

  .send-btn {
    margin-top: 20px;
  }

  textarea {
    font-size: 1.1rem;
  }

  @media ${devices.desktop} {
    .send-btn {
      margin-top: 0px;
    }

    textarea {
      margin: -20px 30px 0px 0px;
    }
  }
`

const EditComment = ({id, content, replyingTo, commentLevel, stopEditing}: EditCommentPropsType) => {
    const [comment, setComment] = useState(replyingTo ? `@${replyingTo} ${content}` : `${content}`)
    const [, dispatch] = useStateValue()

    const handleSend = () => {
        /* eslint-disable no-useless-escape */
        // remove @replyingTo name from the comment body
        const r = new RegExp(`@${replyingTo}\s*`, 'gm')
        const filteredContent = comment.replace(r, '').replace(/(^[,\s]+)|([,\s]+$)/g, '')


        dispatch(editComment(id, filteredContent, commentLevel))
        stopEditing()
    }

    return <EditCommentContainerStyles>
                <textarea
                    onChange={(e) => setComment(e.target.value)}
                    value={comment}
                >
                </textarea>
        <div className="send-btn" onClick={handleSend}>Update</div>

    </EditCommentContainerStyles>
}

export default EditComment