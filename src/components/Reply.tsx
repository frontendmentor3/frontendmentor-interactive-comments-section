import React, {useState} from 'react'
import {addReply, useStateValue} from '../state';
import {getAvatarFileName} from '../utils/getAvatarFilename';
import {EditCommentStyles} from '../styles/EditCommentStyles';
import styled from 'styled-components';


type ReplyPropsType = {
    replyTo: string,
    parentId: number,
    parentLevel: number,
    closeReply: ()=>void
}

const ReplyCommentStyles = styled(EditCommentStyles)`
  width: 93%;  
`

const Reply = ({replyTo, parentId, parentLevel, closeReply}:ReplyPropsType) => {
    const [{currentUser}, ] = useStateValue()
    const [comment, setComment] = useState(`@${replyTo} `)
    const [,dispatch] = useStateValue()

    const handleSend = ()=>{
        /* eslint-disable no-useless-escape */
        // remove @replyTo name from the comment body
        const r = new RegExp(`@${replyTo}\s*` ,'gm')
        const filteredContent = comment.replace(r,'').replace(/(^[,\s]+)|([,\s]+$)/g, '')

        const newComment = {
            id: Math.floor(Math.random() * 1000000),
            content: filteredContent,
            createdAt: 'now',
            score: 0,
            replyingTo: replyTo,
            user: currentUser!!
        }
        dispatch(addReply(newComment, parentId, parentLevel))
        setComment('')
        closeReply()
    }


    return (
        <ReplyCommentStyles>
            {currentUser?(<>
                    <img className="avatar"
                         src={`/assets/images/avatars/${getAvatarFileName(currentUser.image.png)}`}
                         alt={`${currentUser.username}-avatar`}/>
                    <textarea
                        onChange={(e)=>setComment(e.target.value)}
                        value={comment}
                        placeholder="Add a comment..."></textarea>
                    <div className="send-btn" onClick={handleSend}>SEND</div>
                </>)
                :"You're not logged in."}
        </ReplyCommentStyles>
    )

}

export default Reply