import {CommentType} from '../types/MessageData';
import React, {useState} from 'react';
import {changeVote, useStateValue} from '../state';
import {
    CommentOuterContainer,
    ReplyLine,
    CommentContainer,
    ReplyContainer
} from '../../src/styles/CommentStyles'
import {getAvatarFileName} from '../utils/getAvatarFilename';
import Reply from './Reply';
import EditComment from './EditComment';


type CommentPropsType = {
    comment: CommentType,
    replyLevel: number,
    handleDelete: (id: number, level: number) => void
}

const Comment = ({
                     comment: {
                         id,
                         score,
                         user,
                         content,
                         createdAt,
                         replies,
                         replyingTo
                     },
                     replyLevel,
                     handleDelete,
                 }: CommentPropsType,
) => {

    const [{currentUser},] = useStateValue()
    const [, dispatch] = useStateValue()
    const [reply, setReply] = useState(false)
    const [isEditing, setIsEditing] = useState(false)

    return (
        <CommentOuterContainer>
            {replyLevel === 1 && <ReplyLine/>}
            <CommentContainer replyLevel={replyLevel}>
                <div className="vote">
                    <img className="upvote"
                         src={`/assets/images/icon-plus.svg`}
                         alt="upvote"
                         onClick={() => dispatch(changeVote(id, replyLevel, 1))}/>
                    <div className="score">{score}</div>
                    <img className="downvote"
                         src={`/assets/images/icon-minus.svg`}
                         alt="downvote"
                         onClick={() => dispatch(changeVote(id, replyLevel, -1))}
                    />
                </div>
                <div className="reply-del-edit-div">
                    {currentUser?.username === user?.username ? (
                        <div className="edit-delete">
                            <div className="del-button-div"
                                 onClick={() => handleDelete(id, replyLevel)}>
                                <img src={`/assets/images/icon-delete.svg`}
                                     alt="delete"/>
                                <span className="del-text">Delete</span></div>
                            <div className="edit-button-div"
                                 onClick={() => setIsEditing(!isEditing)}
                            >
                                <img src={`/assets/images/icon-edit.svg`}
                                     alt="edit"/>
                                <span className="edit-text">Edit</span>
                            </div>
                        </div>

                    ) : (
                        <div className="reply-button-div" onClick={() => setReply(!reply)}><img
                            className="reply-icon"
                            src={`/assets/images/icon-reply.svg`} alt="reply"/>
                            <span className="reply-text">Reply</span></div>

                    )}

                </div>
                <div className="comment-header">
                    <img className="avatar"
                         src={`/assets/images/avatars/${getAvatarFileName(user?.image.png)}`}
                         alt={`${user?.username}-avatar`}/>
                    <div className="poster-username">{user?.username}
                        {currentUser?.username === user?.username ? <span className="you-tag">you</span> : null}
                    </div>
                    <div className="post-created">{createdAt}</div>
                </div>
                {isEditing ? <EditComment id={id}
                                          content={content}
                                          replyingTo={replyingTo}
                                          commentLevel={replyLevel}
                                          stopEditing={() => setIsEditing(false)}
                    /> :
                    <div className="post-content">{replyingTo ?
                        <span className="replying-to">{`@${replyingTo}`}</span> : ``} {content}</div>
                }
            </CommentContainer>

            <ReplyContainer>
                {reply && <Reply
                    replyTo={user.username}
                    parentId={id}
                    parentLevel={replyLevel}
                    closeReply={() => setReply(false)}
                />}
            </ReplyContainer>


            <br/>
            {replies?.map(comment =>
                <Comment key={comment.id}
                         comment={comment}
                         replyLevel={replyLevel + 1}
                         handleDelete={handleDelete}
                />)}

        </CommentOuterContainer>)
}

export default Comment