import {State} from './state'
import {CommentType, UserType} from '../types/MessageData';
import {getComments, getCurrentUser} from '../services/messages'

export type Action =
    | {
    type: 'FETCH_CURRENT_USER'
} | {
    type: 'SET_CURRENT_USER'
    payload: UserType
} | {
    type: 'FETCH_ALL_COMMENTS'
} | {
    type: 'CHANGE_VOTE'
    payload: VotePayload
} | {
    type: 'DELETE_COMMENT'
    payload: CommentPayload
} | {
    type: 'ADD_COMMENT'
    payload: CommentType
} | {
    type: 'ADD_REPLY'
    payload: ReplyPayload
} | {
    type: 'EDIT_COMMENT'
    payload: EditCommentPayload
}

type VotePayload = {
    id: number,
    level: number,
    inc: number,
}

type CommentPayload = {
    id: number,
    level: number,
}

type ReplyPayload = {
    newComment: CommentType,
    parentId: number,
    parentLevel: number
}

type EditCommentPayload = {
    id: number,
    newContent: string,
    commentLevel: number
}

export const reducer = (state: State, action: Action): State => {
    switch (action.type) {
        case 'FETCH_CURRENT_USER':
            return {
                ...state,
                currentUser: getCurrentUser()
            }
        case 'SET_CURRENT_USER':
            return {
                ...state,
                currentUser: action.payload
            }
        case 'FETCH_ALL_COMMENTS':
            return {
                ...state,
                comments: getComments()
            }
        case 'CHANGE_VOTE':
            return handleVoteChange(state, action.payload)
        case 'DELETE_COMMENT':
            return handleDeleteComment(state, action.payload)
        case 'ADD_COMMENT':
            return handleAddComment(state, action.payload)
        case 'ADD_REPLY':
            return handleReply(state, action.payload)
        case 'EDIT_COMMENT':
            return handleEditComment(state, action.payload)
        default:
            return state;
    }
}

export const setCurrentUser = (currentUser: UserType): Action => {
    return {
        type: 'SET_CURRENT_USER',
        payload: currentUser
    }
}

export const fetchCurrentUser = (): Action => {
    return {
        type: 'FETCH_CURRENT_USER',
    }
}

export const fetchComments = (): Action => {
    return {
        type: 'FETCH_ALL_COMMENTS'
    }
}

export const changeVote = (id: number, level: number, inc: number): Action => {
    return {
        type: 'CHANGE_VOTE',
        payload: {id, level, inc}
    }
}

export const deleteComment = (id: number, level: number): Action => {
    return {
        type: 'DELETE_COMMENT',
        payload: {id, level}
    }
}

export const addComment = (comment: CommentType): Action => {
    return {
        type: 'ADD_COMMENT',
        payload: comment
    }
}

export const addReply = (comment: CommentType,
                         parentId: number,
                         parentLevel: number): Action => {
    return {
        type: 'ADD_REPLY',
        payload: {newComment: comment, parentId, parentLevel}
    }
}

export const editComment = (id: number,
                            newContent: string,
                            commentLevel: number): Action => {
    return {
        type: 'EDIT_COMMENT',
        payload: {id, newContent, commentLevel}
    }
}

const handleVoteChange = (state: State, {id, level, inc}: VotePayload): State => {
    const comments = state.comments
    if (level === 0) {
        comments?.forEach(c => {
            if (c.id === id) {
                c.score += inc
            }
            return c
        })
    } else if (level === 1) {
        comments?.forEach(c => {
            c.replies?.forEach(reply => {
                if (reply.id === id) {
                    reply.score += inc
                }
            })
        })
    }
    return {...state, comments}
}

const handleDeleteComment = (state: State, {id, level}: CommentPayload): State => {
    const comments = state.comments

    if (level === 0) {
        const newComments = comments?.filter(r => r.id !== id)
        return {...state, comments: newComments!!}
    } else if (level === 1) {
        comments?.filter(c => (c.replies = c.replies?.filter(r => r.id !== id)))

    }
    return {...state, comments}
}

const handleAddComment = (state: State, comment: CommentType): State => {
    const newComments = state.comments?.concat(comment)
    return {...state, comments: newComments!!}
}

const handleReply = (state: State,
                     {newComment, parentId, parentLevel}: ReplyPayload): State => {

    if (parentLevel === 0) {
        const newComments = state?.comments?.map(c => {
            return (c.id === parentId) ?
                {...c, replies: [...c?.replies!!, newComment]}
                : c
        })
        return {...state, comments: newComments!!}
    } else if (parentLevel === 1) {
        const newComments = state?.comments?.map(c => {
            return (c?.replies?.some(r => r.id === parentId)) ?
                {...c, replies: [...c?.replies!!, newComment]}
                : c
        })
        return {...state, comments: newComments!!}
    }
    return state
}

const handleEditComment = (state: State, {id, newContent, commentLevel}: EditCommentPayload): State => {
    if (commentLevel === 0) {
        const newComments = state?.comments?.map(c => {
            return (c.id === id) ?
                {...c, content: newContent}
                : c
        })
        return {...state, comments: newComments!!}
    } else if (commentLevel === 1) {

        const newComments = state.comments?.map(c => {
            return (c.replies?.some(r => r.id === id)) ?
                {
                    ...c, replies: c.replies.map(reply => {
                        return (reply.id === id) ?
                            {...reply, content: newContent}
                            : reply
                    })
                }
                : c
        }) as CommentType[]

        return {...state, comments: newComments}
    }

    return state
}