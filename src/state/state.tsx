import React, {createContext, useContext, useReducer} from 'react';
import {Action} from './reducer'
import {CommentType, UserType} from '../types/MessageData';

export type State = {
    currentUser: UserType | null,
    comments: CommentType[] | null,
}

type StateProviderProps = {
    reducer: React.Reducer<State, Action>;
    children: React.ReactElement;
}

const initialState: State = {
    currentUser: null,
    comments: null,
}

export const StateContext = createContext<[State, React.Dispatch<Action>]>([initialState,
    () => initialState,])

export const StateProvider = ({reducer, children}: StateProviderProps) => {
    const [state, dispatch] = useReducer(reducer, initialState)
    return (
        <StateContext.Provider value={[state, dispatch]}>
            {children}
        </StateContext.Provider>
    )
}

export const useStateValue = () => useContext(StateContext)