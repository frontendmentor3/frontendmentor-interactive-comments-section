export type UserImageType = {
    png: string,
    webp: string
}

export type UserType = {
    image: UserImageType
    username: string
}

export type CommentType = {
    id: number,
    content: string,
    createdAt: string,
    score: number,
    replyingTo?: string,
    user: UserType,
    replies?: CommentType[]|[],
}

export type CommentDataType = {
    currentUser: UserType,
    comments: CommentType[]
}