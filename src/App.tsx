import React, {useEffect, useState} from 'react';
import Comment from './components/Comment';
import SendComment from './components/SendComment';
import {fetchCurrentUser, fetchComments, useStateValue} from './state';
import {DeleteModal} from './components/DeleteModal';
import styled from 'styled-components';

const AppStyles = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

function App() {
    const [{comments},] = useStateValue()
    const [, dispatch] = useStateValue()
    const [deleteCommentId, setDeleteCommentId] = useState(-1)
    const [deleteCommentLevel, setDeleteCommentLevel] = useState(-1)
    const [showDeleteModal, setShowDeleteModal] = useState(false)

    const handleDelete = (commentID: number, level: number) => {
        setDeleteCommentId(commentID)
        setDeleteCommentLevel(level)
        setShowDeleteModal(!showDeleteModal)
    }

    useEffect(() => {
        dispatch(fetchCurrentUser())
    }, [dispatch])

    useEffect(() => {
        dispatch(fetchComments())
    }, [dispatch])


    return (
        <AppStyles>

            {/*console.log(`current user: `)}
        {console.log(currentUser)}
        {console.log(`comments: `)}
        {console.log(comments)*/}


            {comments?.map(comment =>
                <Comment key={comment.id}
                         comment={comment}
                         replyLevel={0}
                         handleDelete={handleDelete}/>
            )}

            {showDeleteModal && <DeleteModal
                commentId={deleteCommentId}
                level={deleteCommentLevel}
                setShow={setShowDeleteModal}/>}
            <SendComment/>

        </AppStyles>
    );
}

export default App;
