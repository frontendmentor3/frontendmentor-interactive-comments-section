import {createGlobalStyle} from 'styled-components';
import {fontSize} from './Sizes';

export const GlobalStyles = createGlobalStyle`
  :root{
    --primary-moderate-blue: hsl(238, 40%, 52%);
    --primary-soft-red: hsl(358, 79%, 66%);
    --primary-light-grayish-blue: hsl(239, 57%, 85%);
    --primary-pale-red: hsl(357, 100%, 86%);

    --secondary-dark-blue: hsl(212, 24%, 26%);
    --secondary-grayish-blue: hsl(211, 10%, 45%);
    --secondary-light-gray: hsl(223, 19%, 93%);
    --secondary-very-light-gray: hsl(228, 33%, 97%);
    
    --white: hsl(180,0%,100%);
    --black: hsl(0,0%,0%);

    font-family: 'Rubik', sans-serif;
    font-size: ${fontSize.primary};
    background-color: var(--secondary-light-gray);
  }
`