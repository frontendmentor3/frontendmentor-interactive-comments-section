import styled from 'styled-components';
import {devices} from './Sizes';

export const EditCommentStyles = styled.div`
  display: grid;
  grid-template-areas: 
    "comment comment"
    "avatar send";
  align-items: center;
  justify-content: space-between;
  min-width: 350px;
  max-width: 1000px;
  margin: 20px 20px;
  padding: 20px;
  border-radius: 10px;
  background-color: var(--white);

  textarea {
    grid-area: comment;
    border-radius: 10px;
    border: 1px solid var(--secondary-light-gray);
    height: 120px;
    width: 100%;
    resize: vertical;
    font-family: 'Rubik', sans-serif;
    font-size: 1.1rem;
    padding: 10px 15px;
    box-sizing: border-box;

  }

  .avatar {
    grid-area: avatar;
    width: 35px;
    margin: 20px 0px;
  }

  .send-btn {
    background-color: var(--primary-moderate-blue);
    color: var(--white);
    cursor: pointer;
    font-family: 'Rubik', sans-serif;
    padding: 13px 20px;
    border-radius: 8px;
    text-align: center;
    width: 70px;
    justify-self: end;
  }

  .send-btn:hover {
    background-color: var(--primary-light-grayish-blue)
  }

  @media ${devices.desktop} {
    display: flex;
    align-items: flex-start;

    .avatar {
      margin: 0px;
    }

    textarea {
      margin: 0px 30px;
    }

  }
`