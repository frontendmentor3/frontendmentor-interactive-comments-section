import styled from 'styled-components';
import {devices} from './Sizes';

export const CommentOuterContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-end;
  width: 100%;
  min-width: 350px;
  max-width: 1000px;
  margin: 0px 10px;
`

export const ReplyLine = styled.div`
  flex-basis: 4px;
  background-color: var(--primary-light-grayish-blue);
  margin-right: 20px;
`


export const CommentContainer = styled.div<{ replyLevel: number }>`
  flex-basis: ${props => props.replyLevel === 1 ? '80%' : '100%'};
  display: grid;
  grid-template-areas:
      "header header"
      "content content"
      "vote reply";
  gap: 20px;
  padding: 20px;
  border-radius: 10px;
  max-width: 90%;
  margin: 0 auto;
  background-color: var(--white);

  .comment-header {
    grid-area: header;
    display: flex;
    flex-direction: row;
    align-items: center;
    gap: 15px;
  }

  .avatar {
    width: 35px;
  }

  .poster-username {
    color: var(--secondary-dark-blue);
    font-weight: 700;
  }

  .you-tag {
    background-color: var(--primary-moderate-blue);
    color: var(--white);
    padding: 3px 7px;
    border-radius: 3px;
    margin-left: 10px;
    font-weight: normal;
    font-size: 0.8rem;
    line-height: 0.8rem;
  }

  .post-created {
    color: var(--secondary-grayish-blue);
  }

  .replying-to {
    color: var(--primary-moderate-blue);
    font-weight: bold;
  }

  .post-content {
    grid-area: content;
    color: var(--secondary-grayish-blue);
    line-height: 1.5;
    word-break: break-word;
  }

  .vote {
    grid-area: vote;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 10px;
    margin-right: auto;
    background-color: var(--secondary-very-light-gray);
    color: var(--primary-moderate-blue);
    font-weight: 500;
    font-size: 1.1rem;
    max-height: 100px;
  }

  .upvote, .downvote {
    cursor: pointer;
  }

  .upvote:hover, .downvote:hover {
    filter: invert(80%);
  }

  .vote > img {
    padding: 15px;
  }

  .reply-del-edit-div {
    grid-area: reply;
    margin-left: auto;
    align-self: center;
  }

  .del-button-div, .edit-button-div, .reply-button-div {
    cursor: pointer;
    display: flex;
    align-items: center;
    
    img{
      height: 14px;
      width: auto;
    }
  }

  .reply-text, .edit-text, .del-text {
    color: var(--primary-moderate-blue);
    font-weight: 500;
    font-size: 1.1rem;
  }

  .del-text {
    color: var(--primary-soft-red)
  }


  .edit-delete {
    display: flex;
    gap: 30px;
  }

  .reply-icon, .del-button-div > img, .edit-button-div > img {
    margin-right: 10px;
  }


  @media ${devices.desktop} {
    flex-basis: ${props => props.replyLevel === 1 ? '90%' : '100%'};
    display: grid;
    grid-template-columns: 50px auto auto;
    grid-template-areas:
      "vote header reply"
      "vote content content";

    .reply-div {
      grid-area: reply;
    }

    .vote {
      grid-area: vote;
      flex-direction: column;
    }

    .comment-header {
      grid-area: header;
    }

    .post-content {
      grid-area: content;
      margin-right: auto;
      word-break: break-word;
    }
  }
`

export const ReplyContainer = styled.div`
  width: 100%;
  margin: 0 25px;
`