export const getAvatarFileName = (path: String): String => {
    return path?.split("/").pop() ?? ""
}