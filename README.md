# Frontend Mentor - Interactive comments section solution

This is a solution to the [Interactive comments section challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/interactive-comments-section-iG1RugEG9). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)


## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Create, Read, Update, and Delete comments and replies
- Upvote and downvote comments
- **Bonus**: If you're building a purely front-end project, use `localStorage` to save the current state in the browser that persists when the browser is refreshed.
- **Bonus**: Instead of using the `createdAt` strings from the `data.json` file, try using timestamps and dynamically track the time since the comment or reply was posted.

### Screenshot

[![](https://i.imgur.com/199UdmZm.jpg)](https://i.imgur.com/199UdmZ.png)
[![](https://i.imgur.com/3cGHNPsm.jpg)](https://i.imgur.com/3cGHNPs.png)
[![](https://i.imgur.com/QRQfTuam.jpg)](https://i.imgur.com/QRQfTua.png)


### Links

- Solution URL: [Gitlab](https://gitlab.com/frontendmentor3/frontendmentor-interactive-comments-section)
- Live Site URL: [Netlify](https://heartfelt-kitsune-62f419.netlify.app/)

## My process

### Built with

- Flexbox
- CSS Grid
- Mobile-first workflow
- [React](https://reactjs.org/) - JS library
- Typescript.js
- [Styled Components](https://styled-components.com/) - For styles
- useContext and useReducer Hooks - For state management

### What I learned

REACT_APP_PUBLIC_URL is not needed unless for a custom url.
By default, react will know the asset is in /public/ directly
```js
src={`/assets/images/avatars/${getAvatarFileName(currentUser.image.png)}`}
```
No need to append ${process.env.REACT_APP_PUBLIC_URL} unless using customized public url

### Continued development/TODO
New Features:
- [ ] **Bonus**: If you're building a purely front-end project, use `localStorage` to save the current state in the browser that persists when the browser is refreshed.
- [ ] **Bonus**: Instead of using the `createdAt` strings from the `data.json` file, try using timestamps and dynamically track the time since the comment or reply was posted.

Fixes needed:
- [ ] Re-write some functions in state reducer to not mutate the original state
- [x] Better lineup of purple bars, comments and 'Add a comment' in desktop mode
- [x] Reduce max-width 
- [x] reply, delete, edit icon looks squished

### Useful resources

- [How To Share State Across React Components with Context](https://www.digitalocean.com/community/tutorials/how-to-share-state-across-react-components-with-context)
- [React useContext and useReducer Hooks](https://dev.to/eswaraprakash/react-usecontext-and-usereducer-hooks-2pkm#:~:text=Instead%20of%20passing%20the%20props,function%2C%20and%20an%20initial%20state.) 
- [How to deploy a React application to Netlify](https://www.freecodecamp.org/news/how-to-deploy-a-react-application-to-netlify-363b8a98a985/)

## Author

- Website - [Cheryl M](cherylm.dev)
- Github - [cherylli](https://github.com/cherylli)
- Gitlab - [cherylm](https://gitlab.com/cherylm)
- Frontend Mentor - [@cherylli](https://www.frontendmentor.io/profile/cherylli)


